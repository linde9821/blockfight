﻿using Audio.AudioManager;
using GameManagment;
using GameObjects.ManagementObjects;
using UI;
using Zenject;

namespace InvasionOfControll
{
    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<GameManager>().FromComponentsInHierarchy().AsSingle();
            Container.Bind<IngameUI>().FromComponentsInHierarchy().AsSingle();
            Container.Bind<AudioManager>().FromComponentsInHierarchy().AsSingle();
            Container.Bind<RoundManager>().FromComponentsInHierarchy().AsSingle();
            Container.Bind<SpawnPointsComponent>().FromComponentsInHierarchy().AsSingle();
        }
    }
}
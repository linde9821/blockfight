﻿using Data;
using Gameplay.Player;
using TMPro;
using UnityEngine;

namespace UI
{
    public class IngameUI : MonoBehaviour
    {
        public HealthBar[] healthBars;
        public TMP_Text message;
        private int _playersCounter;

        public void Reset()
        {
            _playersCounter = 0;
            foreach (var bar in healthBars) bar.Reset();
        }

        private void Update()
        {
            message.enabled = DataStore.PlayerIsMissing;
        }

        public void AddPlayer(Player playerValue)
        {
            healthBars[_playersCounter].AddPlayer(playerValue);
            _playersCounter++;
        }
    }
}
using Data;
using GameManagment;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class MainMenu : MonoBehaviour
    {
        public GameObject mainMenu;
        public GameObject resultsMenu;
        public TMP_Text winnerName;
        public TMP_Text winnerPoints;
        private int _bestOfCount = 3;
        private int _mapIndex = 1;
        private int _playerCount = 2;

        public void Start()
        {
            // When Scene Is loaded after a Game finished, the ResultsScreen is shown instead of MainMenu
            if (!DataStore.GameFinished) return;

            DataStore.GameFinished = false;
            mainMenu.SetActive(false);
            resultsMenu.SetActive(true);
            SetWinner();
        }

        public void PlayGame()
        {
            DataStore.RoundConfig = new RoundConfig(_bestOfCount, _playerCount);
            SceneManager.LoadScene(_mapIndex); // no need to save map anywhere
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        public void SetMap(int mapIndex)
        {
            _mapIndex = mapIndex + 1;
        }

        public void SetRoundsCount(int roundsCounts)
        {
            _bestOfCount = roundsCounts * 2 + 1;
        }

        public void SetPlayerCount(int playerCountIndex)
        {
            _playerCount = playerCountIndex + 2;
        }

        private void SetWinner()
        {
            winnerName.SetText(DataStore.Name);
            winnerName.color = DataStore.Color;
            winnerPoints.SetText(DataStore.Points.ToString());
            winnerPoints.color = winnerName.color;
        }
    }
}
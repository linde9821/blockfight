﻿using System.Collections.Generic;
using ModestTree;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;

namespace UI
{
    public class OptionsMenu : MonoBehaviour
    {
        public AudioMixer mainMixer;
        public TMP_Dropdown resolutionDropdown;
        private Resolution[] _resolutions;

        private void Start()
        {
            _resolutions = Screen.resolutions;

            resolutionDropdown.ClearOptions();
            var options = new List<string>();
            var currentResolutionIndex = 0;
            foreach (var res in _resolutions)
            {
                var option = res.width + "x" + res.height;
                options.Add(option);

                if (res.width == Screen.currentResolution.width &&
                    res.height == Screen.currentResolution.height)
                    currentResolutionIndex = _resolutions.IndexOf(res);
            }

            resolutionDropdown.AddOptions(options);
            resolutionDropdown.value = currentResolutionIndex;
            resolutionDropdown.RefreshShownValue();
        }

        public void SetResolution(int resolutionIndex)
        {
            var res = _resolutions[resolutionIndex];
            Screen.SetResolution(res.width, res.height, Screen.fullScreen);
        }

        public void SetVolume(float volume)
        {
            if (volume <= -50)
                mainMixer.SetFloat("masterVolume",
                    -80); // no heareble difference between -50 and -80, reduce length of Slider
            else
                mainMixer.SetFloat("masterVolume", volume);
        }

        public void SetGraphics(int qualityIndex)
        {
            QualitySettings.SetQualityLevel(qualityIndex);
        }

        public void SetFullscreen(bool isFullscreen)
        {
            Screen.fullScreen = isFullscreen;
        }
    }
}
﻿using Gameplay.Player;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HealthBar : MonoBehaviour
    {
        public GameObject playerHealthBar;
        public TMP_Text nameText;
        public TMP_Text pointsText;
        public Image[] hearts;
        public Sprite fullHeart;
        public Sprite emptyHeart;
        public Slider livesOfLifeSlider;
        public Gradient gradient;
        public Image fillHealthBar;
        public Image fillDashBar;
        public Slider dashCooldownSlider;
        private int _lives;
        private int _maxLives; // possible to implement pick up lives to increase maxLives of a player
        private Player _player;

        public void Reset()
        {
            playerHealthBar.SetActive(false);
            _player = null;
            _maxLives = 2;
            _lives = 2;
            livesOfLifeSlider.maxValue = 10;
            livesOfLifeSlider.value = 10;
            dashCooldownSlider.maxValue = 1;
            dashCooldownSlider.value = 1;
            nameText.SetText("Name");
            nameText.color = Color.white;
            pointsText.SetText("Points");
            pointsText.color = Color.white;
        }

        private void Update()
        {
            SetLives();
            SetLivesOfLive();
            SetRemainDashCooldown();
            SetDisplayPoints();
        }

        public void AddPlayer(Player playerValue)
        {
            _player = playerValue;
            SetDisplayName();
            SetMaxLives();
            SetMaxLivesOfLive();
            SetMaxDashCooldown();
            playerHealthBar.SetActive(true);
        }

        // takes the players Max lives (<=5) and displays that many Heart-Images
        private void SetMaxLives()
        {
            var maxLives = _player.playerStateValues.lifes;
            _maxLives = maxLives > 5 ? 5 : maxLives;
            for (var i = 0; i < hearts.Length; i++) hearts[i].enabled = i < _maxLives;
        }

        // gets the actual health of the Player and displays that many of the shown images as full Hearts
        private void SetLives()
        {
            var lives = _player.currentPlayerState.Lifes;
            _lives = lives > _maxLives ? _maxLives : lives;

            for (var i = 0; i < hearts.Length; i++) hearts[i].sprite = i < _lives ? fullHeart : emptyHeart;
        }

        private void SetMaxLivesOfLive() // max HealthBar fill
        {
            var maxLivesOfLive = _player.playerStateValues.lifePointsPerLife;
            livesOfLifeSlider.maxValue = maxLivesOfLive;
        }

        private void SetLivesOfLive() // amount of HealthBar filled
        {
            var livesOfLive = _player.currentPlayerState.CurrentLifesLifepoints;
            livesOfLifeSlider.value = livesOfLive;
            fillHealthBar.color = gradient.Evaluate(livesOfLifeSlider.normalizedValue);
        }

        private void SetMaxDashCooldown()
        {
            var minCooldown = _player.playerStateValues.dashCooldown;
            dashCooldownSlider.minValue = 0;
            dashCooldownSlider.maxValue = minCooldown;
            fillDashBar.color = Color.blue;
        }

        private void SetRemainDashCooldown()
        {
            var restored = 1 - _player.movementController.dashCooldownTimer;
            dashCooldownSlider.value = restored > dashCooldownSlider.maxValue ? dashCooldownSlider.maxValue : restored;
        }

        private void SetDisplayName()
        {
            nameText.SetText(_player.name);
            nameText.color = _player.color;
            pointsText.color = _player.color;
        }

        private void SetDisplayPoints()
        {
            pointsText.SetText(_player.currentPlayerState.CurrentPoints.ToString());
        }
    }
}
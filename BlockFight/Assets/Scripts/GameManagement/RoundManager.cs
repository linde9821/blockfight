﻿using System.Collections.Generic;
using System.Linq;
using Data;
using Gameplay.Player;
using UI;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.SceneManagement;
using Zenject;

namespace GameManagment
{
    /// <summary>
    ///     A class which handles the "round gameplay loop" and the associated state.
    ///     Is configured to be injectable (as Singleton)
    /// </summary>
    [DisallowMultipleComponent]
    public class RoundManager : MonoBehaviour
    {
        private readonly List<Color> _colors;
        private readonly Dictionary<Color, string> _colorToString;
        private int _colorIndex;
        private GameManager _gameManager;
        private IngameUI _ingameUI;
        private Dictionary<GameObject, Player> _players;
        private RoundConfig _roundConfig;
        private List<RoundState> _roundStates;

        public RoundManager()
        {
            Init();
            _colors = new List<Color> {Color.green, Color.cyan, Color.red, Color.yellow};
            _colorToString = new Dictionary<Color, string>
                {{Color.green, "Green"}, {Color.cyan, "Cyan"}, {Color.red, "Red"}, {Color.yellow, "Yellow"}};
        }

        // LateUpdate to ensure that player prefab is instantiated (takes time because of input system)
        private void LateUpdate()
        {
            if (_gameManager.GameIsRunning) return;

            if (_players.Count >= _roundConfig.Players)
            {
                DataStore.PlayerIsMissing = false;
                StartRound(true);
            }
            else
            {
                DataStore.PlayerIsMissing = true;
            }
        }

        private string ColorToString(Color color)
        {
            return _colorToString[color];
        }

        private void Init()
        {
            _players = new Dictionary<GameObject, Player>();
            _roundStates = new List<RoundState>();
            _roundConfig = DataStore.RoundConfig;
            _colorIndex = 0;
        }

        private Color NextColor()
        {
            var color = _colors[_colorIndex];
            _colorIndex++;
            return color;
        }

        [Inject]
        public void Initialize(GameManager gameManager, IngameUI ingameUI)
        {
            _gameManager = gameManager;
            _ingameUI = ingameUI;
        }

        /// <summary>
        ///     Handles created Player (Injection provided by Zenject, instantiation by InputSystem)
        ///     Starts round when enough players are there
        /// </summary>
        private void OnPlayerJoined(Component playerInput)
        {
            if (_players.Count >= _roundConfig.Players)
            {
                Debug.LogWarning("Rejected next Player");
                return;
            }

            InitPlayer(playerInput);
        }

        private void InitPlayer(Component playerInput)
        {
            playerInput.gameObject.SetActive(true);
            var player = playerInput.gameObject.GetComponent<Player>();
            var color = NextColor();
            var spriteRenderer = playerInput.GetComponent<SpriteRenderer>();
            spriteRenderer.color = color;
            spriteRenderer.enabled = false;
            playerInput.GetComponent<Light2D>().color = color;
            player.color = color;
            player.name = "Player" + ColorToString(color);
            player.transform.position = new Vector3(10000, 1000, 10000);
            _players.Add(playerInput.gameObject, player);
        }

        /// <summary>
        ///     Handles RoundEnd (invoked by GameManger)
        /// </summary>
        public void OnRoundEnd(RoundState roundState)
        {
            Debug.LogWarning(roundState.RoundDescription);
            _roundStates.Add(roundState);

            UpdateAllPlayerPoints();

            if (CheckForGameEnding())
            {
                Debug.LogWarning("Game Ended");
                var winner = CalculateWinner();
                Debug.LogWarning($"The Winner is {winner.name}");
                DataStore.Color = winner.color;
                DataStore.Name = winner.name;
                DataStore.Points = winner.currentPlayerState.CurrentPoints;
                _ingameUI.Reset();
                DestroyPlayers();
                Init();
                DataStore.GameFinished = true; // tells the MainMenu to start with resultScreen
                SceneManager.LoadScene(0);
            }
            else
            {
                StartRound(false);
            }
        }

        private void UpdateAllPlayerPoints()
        {
            foreach (var player in _players.Values) player.currentPlayerState.CurrentPoints = RoundsWonByPlayer(player);
        }

        /// <summary>
        ///     Destroys all Player GameObjects
        /// </summary>
        private void DestroyPlayers()
        {
            foreach (var player in _players.Keys) Destroy(player.gameObject);
        }

        /// <summary>
        ///     Invokes RoundStart in GameManager
        /// </summary>
        private void StartRound(bool isFirstRound)
        {
            Debug.LogWarning("Starting new round");
            foreach (var player in _players.Keys) player.GetComponent<Renderer>().enabled = true;
            _gameManager.StartRound(_players, isFirstRound);
        }

        /// <summary>
        ///     Check if game has ended. Either because no rounds are left or one player has one more rounds than half of the
        ///     available rounds
        /// </summary>
        private bool CheckForGameEnding()
        {
            if (_roundStates.Count >= _roundConfig.BestOf) return true;

            var roundsToWin = (_roundConfig.BestOf - 1) / 2;

            return _players.Values.Any(player => roundsToWin < RoundsWonByPlayer(player));
        }

        /// <summary>
        ///     Calculate winner
        /// </summary>
        private Player CalculateWinner()
        {
            return _players.Values.OrderBy(RoundsWonByPlayer).Last();
        }

        /// <summary>
        ///     Calculates the rounds a player won via a fold Higher Order Function
        /// </summary>
        private int RoundsWonByPlayer(Player playerToCheck)
        {
            return _roundStates.Aggregate(0, (accumulator, state) =>
            {
                if (state.Winner == playerToCheck) return accumulator + 1;

                return accumulator;
            });
        }
    }
}
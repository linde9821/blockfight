﻿using Gameplay.Player;

namespace GameManagment
{
    /// <summary>
    ///     A class which saves the round state
    /// </summary>
    public class RoundState
    {
        public RoundState(Player winner, string roundDescription)
        {
            Winner = winner;
            RoundDescription = roundDescription;
        }

        public Player Winner { get; }

        public string RoundDescription { get; }
    }
}
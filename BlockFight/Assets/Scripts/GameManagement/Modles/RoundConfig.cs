﻿namespace GameManagment
{
    /// <summary>
    ///     A class which handles round config
    /// </summary>
    public class RoundConfig
    {
        public RoundConfig(int bestOf, int players)
        {
            BestOf = bestOf;
            Players = players;
        }

        public int BestOf { get; }

        public int Players { get; }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using GameObjects.ManagementObjects;
using Gameplay.Player;
using UI;
using UnityEngine;
using Zenject;
using Random = System.Random;

namespace GameManagment
{
    /// <summary>
    ///     A class which handles the core gameplay in a scene
    ///     Is configured to be injectable (as Singleton)
    /// </summary>
    [DisallowMultipleComponent]
    public class GameManager : MonoBehaviour
    {
        private HashSet<int> _exclude;
        private IngameUI _ingameUI;
        private string _roundDescription;

        private RoundManager _roundManager;
        private List<SpawnPoint> _spawnPoints;
        public Dictionary<GameObject, Player> Players;
        public bool GameIsRunning { get; private set; }

        private void Start()
        {
            Players = new Dictionary<GameObject, Player>();
            GameIsRunning = false;
            GetComponent<PlayerDeathEvent>().OnPlayerDeath += HandlePlayerDeath;
        }

        private void FixedUpdate()
        {
            if (!GameIsRunning)
                return;

            KillPlayerIfOutsideOfArena();
        }

        [Inject]
        public void Initialize(SpawnPointsComponent spawnPoints, RoundManager roundManager, IngameUI ingameUI)
        {
            _spawnPoints = spawnPoints.spawnPoints;
            _roundManager = roundManager;
            _ingameUI = ingameUI;
        }

        /// <summary>
        ///     Inits and starts round.
        /// </summary>
        public void StartRound(Dictionary<GameObject, Player> players, bool isFirstRound)
        {
            Players = new Dictionary<GameObject, Player>(players);
            _roundDescription = "";
            GameIsRunning = true;
            _exclude = new HashSet<int>();

            foreach (var player in Players)
            {
                SetRandomStartPosition(player.Value);
                player.Value.currentPlayerState.ResetHealth();

                if (isFirstRound) _ingameUI.AddPlayer(player.Value);
            }
        }

        /// <summary>
        ///     Sets a player to a random position from the pinpoints in the scene
        /// </summary>
        private void SetRandomStartPosition(Player player)
        {
            player.transform.position = _spawnPoints[RandomValidSpawnPoint()].Position;
        }

        // https://stackoverflow.com/questions/18484577/how-to-get-a-random-number-from-a-range-excluding-some-values
        private int RandomValidSpawnPoint()
        {
            var range = Enumerable.Range(0, _spawnPoints.Count).Where(i => !_exclude.Contains(i));

            var rand = new Random();
            var index = rand.Next(0, _spawnPoints.Count - _exclude.Count);
            var result = range.ElementAt(index);
            _exclude.Add(result);
            return result;
        }

        /// <summary>
        ///     Kills Player when outside of arena
        /// </summary>
        private void KillPlayerIfOutsideOfArena()
        {
            foreach (var player in Players.Where(player => PlayerIsOutsideOfArena(player.Value)))
                player.Value.currentPlayerState.Kill();
        }

        /// <summary>
        ///     Checks if player is outside of arena
        /// </summary>
        private static bool PlayerIsOutsideOfArena(Component player)
        {
            return player.transform.position.y < -20;
        }

        private void HandlePlayerDeath(Player killedPlayer)
        {
            Debug.LogWarning($"{killedPlayer.name} died");
            Players.Remove(killedPlayer.gameObject);
            _roundDescription += $"{killedPlayer.name} killed\n";
            if (Players.Count <= 1)
                EndLevelRound(Players.Values.Last());
        }

        /// <summary>
        ///     End current round and saves data
        /// </summary>
        private void EndLevelRound(Player lastLivingPLayer)
        {
            GameIsRunning = false;
            _roundDescription += $"The Winner is {lastLivingPLayer.name}";
            _roundManager.OnRoundEnd(new RoundState(lastLivingPLayer, _roundDescription));
        }
    }
}
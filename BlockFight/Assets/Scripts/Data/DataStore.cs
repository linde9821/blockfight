﻿using GameManagment;
using UnityEngine;

namespace Data
{
    public static class DataStore
    {
        public static bool GameFinished;
        public static RoundConfig RoundConfig;

        public static bool PlayerIsMissing;

        public static Color Color;
        public static string Name;
        public static int Points;
    }
}
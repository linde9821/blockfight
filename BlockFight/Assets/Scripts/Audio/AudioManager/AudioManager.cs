﻿using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Serialization;

namespace Audio.AudioManager
{
    /// <summary>
    ///     A class which allows tha music play
    ///     Is configured to be injectable (as Singleton)
    /// </summary>
    public class AudioManager : MonoBehaviour
    {
        private static AudioManager _instance;
        public AudioMixer masterMixer;

        [FormerlySerializedAs("_sounds")] [SerializeField]
        private Sound.Sound[] sounds;

        private void Awake()
        {
            if (_instance == null) _instance = this;

            foreach (var sound in sounds)
            {
                sound.source = gameObject.AddComponent<AudioSource>();
                sound.source.clip = sound.clip;
                sound.source.volume = sound.volume;
                sound.source.pitch = sound.pitch;
                sound.source.loop = sound.loop;
                sound.source.outputAudioMixerGroup = masterMixer.FindMatchingGroups("Master")[0];
            }
        }


        private void Start()
        {
            Play("Theme");
        }

        /// <summary>
        ///     Play a sound by its name
        /// </summary>
        public void Play(string soundName)
        {
            var soundToPlay = Array.Find(sounds, sound => sound.name == soundName);
            if (soundToPlay == null) Debug.LogWarning("Sound: " + soundName + " not found!");
            soundToPlay?.source.Play();
        }
    }
}
﻿using System;
using UnityEngine;

namespace Audio.Sound
{
    /// <summary>
    ///     A class which models a Sound
    /// </summary>
    [Serializable]
    public class Sound
    {
        public AudioClip clip;

        public bool loop;
        public string name;

        [Range(.1f, 3f)] public float pitch;

        [HideInInspector] public AudioSource source;

        [Range(0f, 1f)] public float volume;
    }
}
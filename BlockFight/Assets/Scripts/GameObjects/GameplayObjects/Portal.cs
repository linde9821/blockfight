﻿using Audio.AudioManager;
using UnityEngine;
using Zenject;

public class Portal : MonoBehaviour
{
    // provided from inspector 
    public GameObject portalB;
    private Animator _animatorA;
    private Animator _animatorB;
    private AudioManager _audioManager;
    private Portal _portalBConnector;

    private GameObject _receivedObjectToTeleport;


    private void Start()
    {
        _animatorA = GetComponent<Animator>();
        _animatorB = portalB.GetComponent<Animator>();
        _portalBConnector = portalB.GetComponent<Portal>();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.tag)
        {
            case "Player":
            case "Arrow":

                if (other.gameObject != _receivedObjectToTeleport)
                {
                    _portalBConnector.SetReceivedObject(other.gameObject);
                    TeleportGameObject(other.gameObject);
                }

                break;

            default:
                return;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.Equals(_receivedObjectToTeleport)) _receivedObjectToTeleport = null;
    }

    [Inject]
    private void Initialize(AudioManager audioManager)
    {
        _audioManager = audioManager;
    }

    /// <summary>
    ///     Sets the object which was teleported to this Portal instance to insure it doesnt get teleported back
    /// </summary>
    private void SetReceivedObject(GameObject gameObject)
    {
        _receivedObjectToTeleport = gameObject;
    }

    /// <summary>
    ///     Teleports the GameObject to the other Portal
    /// </summary>
    private void TeleportGameObject(GameObject objectToTeleport)
    {
        _animatorA.Play("Teleport");
        _animatorB.Play("Teleport");
        _audioManager.Play("portal");

        objectToTeleport.transform.position = portalB.transform.position;
    }
}
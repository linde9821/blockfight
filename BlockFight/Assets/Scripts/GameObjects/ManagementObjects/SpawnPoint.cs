﻿using UnityEngine;

namespace GameObjects.ManagementObjects
{
    /// <summary>
    ///     A class modeling a spawnpoint
    /// </summary>
    [RequireComponent(typeof(Transform))]
    public class SpawnPoint : MonoBehaviour
    {
        public Vector2 Position { get; private set; }

        private void Awake()
        {
            Position = GetComponent<Transform>().position;
        }
    }
}
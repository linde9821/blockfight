﻿using System.Collections.Generic;
using UnityEngine;

namespace GameObjects.ManagementObjects
{
    /// <summary>
    ///     A class which holds all spawnpoints
    ///     Is configured to be injectable (as Singleton)
    /// </summary>
    public class SpawnPointsComponent : MonoBehaviour
    {
        public List<SpawnPoint> spawnPoints;
    }
}
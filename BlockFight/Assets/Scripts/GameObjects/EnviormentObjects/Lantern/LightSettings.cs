﻿namespace GameObjects.EnviormentObjects
{
    public class LightSettings
    {
        public LightSettings(float intensity, float outerRadius, float time)
        {
            Intensity = intensity;
            OuterRadius = outerRadius;
            Time = time;
        }

        public float Intensity { get; }

        public float OuterRadius { get; }

        public float Time { get; }
    }
}
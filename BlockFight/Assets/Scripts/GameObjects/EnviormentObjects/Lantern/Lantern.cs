﻿using Audio.AudioManager;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using Zenject;

namespace GameObjects.EnviormentObjects.Lantern
{
    public class Lantern : MonoBehaviour
    {
        private AudioManager _audioManager;

        private bool _isTriggered;
        private Light2D _light;

        private LightSettings _normal;
        private LightSettings _triggered;
        private float _triggerTime;

        private void Start()
        {
            _light = GetComponent<Light2D>();
            _isTriggered = false;

            _triggered = new LightSettings(1.9f, 3.5f, .6f);
            _normal = new LightSettings(1.2f, 2f, 1.0f);
        }

        private void Update()
        {
            if (!_isTriggered) return;

            HandleTrigger();
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            switch (other.tag)
            {
                case "Player":
                case "Arrow":
                    if (!_isTriggered)
                    {
                        _audioManager.Play("Lantern");
                        _isTriggered = true;
                        _triggerTime = Time.realtimeSinceStartup;
                    }

                    break;

                default:
                    return;
            }
        }

        [Inject]
        public void Initialize(AudioManager audioManager)
        {
            _audioManager = audioManager;
        }

        private float IncreaseDelta(float timeSinceTrigger, float startValue, float goalValue)
        {
            var p = timeSinceTrigger / _triggered.Time;
            p = p > 1f ? 1 : p;
            var delta = goalValue - startValue;

            return p * delta + startValue;
        }

        private float DecreaseDelta(float timeSinceTrigger, float startValue, float goalValue)
        {
            var p = (timeSinceTrigger - _triggered.Time) / _normal.Time;
            p = p > 1f ? 1 : p;
            var delta = startValue - goalValue;

            return startValue - p * delta;
        }

        private void HandleTrigger()
        {
            var timeSinceTrigger = Time.realtimeSinceStartup - _triggerTime;

            if (timeSinceTrigger < _triggered.Time)
            {
                _light.intensity = IncreaseDelta(timeSinceTrigger, _normal.Intensity, _triggered.Intensity);
                _light.pointLightOuterRadius =
                    IncreaseDelta(timeSinceTrigger, _normal.OuterRadius, _triggered.OuterRadius);
            }
            else if (timeSinceTrigger < _triggered.Time + _normal.Time)
            {
                _light.intensity = DecreaseDelta(timeSinceTrigger, _triggered.Intensity, _normal.Intensity);
                _light.pointLightOuterRadius =
                    DecreaseDelta(timeSinceTrigger, _triggered.OuterRadius, _normal.OuterRadius);
            }
            else
            {
                _light.intensity = _normal.Intensity;
                _isTriggered = false;
            }
        }
    }
}
﻿using System;
using UnityEngine;

namespace Gameplay.Player
{
    /// <summary>
    ///     A class describing an event to kill a player
    /// </summary>
    public class PlayerDeathEvent : MonoBehaviour
    {
        public static PlayerDeathEvent current;

        private void Awake()
        {
            current = this;
        }

        public event Action<Player> OnPlayerDeath;

        /// <summary>
        ///     Invokes Player Death
        /// </summary>
        public void PlayerDeathTrigger(Player player)
        {
            OnPlayerDeath?.Invoke(player);
        }
    }
}
﻿using Audio.AudioManager;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace Gameplay.Player.Stats
{
    /// <summary>
    ///     A class which handles a players current state
    /// </summary>
    public class CurrentPlayerState : MonoBehaviour
    {
        [FormerlySerializedAs("_currentLifesLifepoints")] [SerializeField]
        private float currentLifesLifepoints;

        [FormerlySerializedAs("_damageResistenceTimer")] [SerializeField]
        private float damageResistenceTimer;

        [FormerlySerializedAs("_lifes")] [SerializeField]
        private int lifes;

        [FormerlySerializedAs("_regenerationTimer")] [SerializeField]
        private float regenerationTimer;

        [FormerlySerializedAs("_currentPoints")] [SerializeField]
        private int currentPoints;

        private AudioManager _audioManager;

        private Player _player;

        private bool _triggerDeathEvent;

        public float CurrentLifesLifepoints => currentLifesLifepoints;

        public float DamageResistenceTimer
        {
            get => damageResistenceTimer;
            set => damageResistenceTimer = value;
        }

        public int Lifes => lifes;

        public int CurrentPoints
        {
            get => currentPoints;
            set => currentPoints = value;
        }

        private void Awake()
        {
            _player = GetComponent<Player>();
            currentPoints = 0;
            ResetHealth();
        }

        private void Update()
        {
            Debug.LogWarning($"{_player.name} update. {lifes}.{currentLifesLifepoints} ({_triggerDeathEvent})");

            RestoreHealthWhenAllowed();

            if (CheckForDeath()) return;

            if (damageResistenceTimer > 0)
                damageResistenceTimer -= Time.deltaTime;
            else
                damageResistenceTimer = 0f;
        }

        [Inject]
        public void Initialize(AudioManager audioManager)
        {
            _audioManager = audioManager;
        }

        private bool HasDamageResistance()
        {
            return damageResistenceTimer > 0f;
        }

        private void RestoreHealthWhenAllowed()
        {
            if (currentLifesLifepoints < _player.playerStateValues.lifePointsPerLife)
            {
                if (regenerationTimer > 0)
                {
                    regenerationTimer -= Time.deltaTime;
                }
                else
                {
                    ResetRestoreTimer();

                    currentLifesLifepoints += _player.playerStateValues.regenerationLifePoints;

                    currentLifesLifepoints = currentLifesLifepoints > +_player.playerStateValues.lifePointsPerLife
                        ? _player.playerStateValues.lifePointsPerLife
                        : currentLifesLifepoints;
                }
            }
        }

        private void ResetRestoreTimer()
        {
            regenerationTimer = _player.playerStateValues.regenerationTime;
        }

        public void DealDamage(float damage)
        {
            Debug.LogWarning(
                $"Player {_player.name} received {damage} damage. Now: {lifes} left and {currentLifesLifepoints} lifeslifepoints");

            if (HasDamageResistance())
            {
                _audioManager.Play("HitResitence");
                return;
            }

            ResetRestoreTimer();

            var newLifePoints = currentLifesLifepoints - damage;

            if (newLifePoints <= 0)
            {
                lifes--;
                currentLifesLifepoints = _player.playerStateValues.lifePointsPerLife;
            }
            else
            {
                currentLifesLifepoints = newLifePoints;
            }
        }

        private bool CheckForDeath()
        {
            if (lifes > 0) return false;

            _player.shootController.SetAimingArrowRenderer(false);

            if (!_triggerDeathEvent) return true;

            _triggerDeathEvent = false;
            PlayerDeathEvent.current.PlayerDeathTrigger(_player);
            _audioManager.Play("dead");

            return true;
        }

        public void Kill()
        {
            lifes = 0;
            currentLifesLifepoints = 0;
        }

        public void ResetHealth()
        {
            lifes = _player.playerStateValues.lifes;
            currentLifesLifepoints = _player.playerStateValues.lifePointsPerLife;
            _triggerDeathEvent = true;
            ResetRestoreTimer();
            Debug.LogWarning($"Resetted {_player.name}s");
        }
    }
}
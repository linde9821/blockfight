﻿using UnityEngine;

namespace Gameplay.Player.Stats
{
    /// <summary>
    ///     A class which models all stats for a players
    /// </summary>
    public class PlayerStateValues : MonoBehaviour
    {
        #region AirMovement

        [Range(0, 1)] public float airControl = .8f;

        #endregion

        #region MovementGeneral

        public float maxMovementSpeed = 4f;

        #endregion

        #region Shoot

        public float shootCooldown = 1f;
        public float shootForce = 9f;

        #endregion

        #region Dash

        public float dashDamageResistenceTime = 1f;
        public float dashCooldown = 1.0f;
        public float dashMovementCooldown = 0.4f;
        public float dashSpeed = 16.5f;

        #endregion

        #region Jump

        public float jumpSpeed = 8.75f;
        public float jumpCooldown = .1f;
        [Range(0, 1)] public float wallHoldingPower = 1.0f;
        public float wallJumpMovementCooldown = .45f;
        public float wallJumpSpeed = 11.5f;

        #endregion

        #region PlayerPlayerCollision

        public float knockBackForce = 10f;
        public float collisionMovementCooldown = .4f;

        #endregion

        #region LifeSystem

        public float lifePointsPerLife = 10;
        public int lifes = 2;
        public float regenerationTime = 8.0f;
        public float regenerationLifePoints = 2;

        #endregion
    }
}
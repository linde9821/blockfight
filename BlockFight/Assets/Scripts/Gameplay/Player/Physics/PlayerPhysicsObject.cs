﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameManagment;
using Gameplay.Player.Movement;
using UnityEngine;
using Zenject;

namespace Gameplay.Player.Physics
{
    /// <summary>
    ///     A class which holds all logic for the custom physics movement
    /// </summary>
    public class PhysicsObject : MonoBehaviour
    {
        private GameManager _gameManager;
        protected Player player;

        private void Start()
        {
            _contactFilter2D.useTriggers = false;
            _contactFilter2D.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
            _contactFilter2D.useLayerMask = true;
            playerHitVelocity = Vector2.zero;
        }

        private void Update()
        {
            ComputeVelocity();
        }

        // handles physic movement 
        private void FixedUpdate()
        {
            if (WantsToHangOnWall)
                Velocity += Physics2D.gravity *
                            (gravityModifier * (1 - player.playerStateValues.wallHoldingPower));
            else
                Velocity += Physics2D.gravity * gravityModifier;

            if (player.playerMovementState.IsInAir() &&
                player.playerMovementState.currentState == PlayerMovementState.States.MovingInAirUp)
                Velocity -= Velocity * airFriction;
            else if (player.playerMovementState.IsInAir() &&
                     player.playerMovementState.currentState == PlayerMovementState.States.MovingInAirDown)
                Velocity.y += Velocity.y * downwardAirspeedMultiplayer;
            else
                Velocity -= Velocity * groundFriction;

            var deltaPosition = Velocity * Time.fixedDeltaTime;

            if (playerHitVelocity.magnitude > 0)
            {
                Velocity = playerHitVelocity;
                deltaPosition = Velocity * Time.fixedDeltaTime;
                playerHitVelocity = Vector2.zero;
            }
            else
            {
                CheckForPlayerCollision(deltaPosition);
                deltaPosition = Velocity * Time.fixedDeltaTime;
            }

            var moveAlongGround = new Vector2(_groundNormal.y, -_groundNormal.x);
            var move = moveAlongGround * deltaPosition.x;
            Movement(move, false);
            move = Vector2.up * deltaPosition.y;
            Movement(move, true);
        }

        private void OnEnable()
        {
            player = GetComponent<Player>();
        }

        [Inject]
        public void Initialize(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        protected virtual void ComputeVelocity()
        {
        }

        private void CheckForPlayerCollision(Vector2 move)
        {
            var count = player.rigidbody2D.Cast(move, _contactFilter2D, _hitBuffer, move.magnitude + ShellRadius);
            _hitBufferList.Clear();

            for (var i = 0; i < count; i++) _hitBufferList.Add(_hitBuffer[i]);

            foreach (var hit in _hitBufferList.Where(hit => hit.transform.gameObject.tag.Equals("Player")))
            {
                SetPlayerPlayerCollisionProjection(hit, move);
                SetPlayersCooldown(hit);
                CheckForHitOnTop(hit);
            }
        }

        private void SetPlayerPlayerCollisionProjection(RaycastHit2D hit, Vector2 move)
        {
            _gameManager.Players[hit.transform.gameObject].movementController.playerHitVelocity =
                move.normalized * player.playerStateValues.knockBackForce;
            Velocity = move.normalized * -player.playerStateValues.knockBackForce;
        }

        private void SetPlayersCooldown(RaycastHit2D hit)
        {
            SetPlayerCooldown(player);
            SetPlayerCooldown(_gameManager.Players[hit.transform.gameObject]);
        }

        private void SetPlayerCooldown(Player playerToSetMovement)
        {
            playerToSetMovement.movementController.currentMovementCooldown +=
                playerToSetMovement.playerStateValues.collisionMovementCooldown;
        }

        private bool CheckForHitOnTop(RaycastHit2D hit)
        {
            var collisionPLayer = _gameManager.Players[hit.collider.gameObject];
            return CheckPlayersAndKillOnHit(player, collisionPLayer) ||
                   CheckPlayersAndKillOnHit(collisionPLayer, player);
        }

        private static bool CheckPlayersAndKillOnHit(Player bottomPlayer, Component topPlayer)
        {
            var collisionPlayerPosition = bottomPlayer.transform.position;
            var playerPosition = topPlayer.transform.position;

            if (collisionPlayerPosition.y + bottomPlayer.height / 2 > playerPosition.y) return false;

            var ab = Math.Abs(collisionPlayerPosition.x - playerPosition.x);
            var bc = Math.Abs(collisionPlayerPosition.y - playerPosition.y);

            var alpha = (float) Math.Atan(bc / ab) * Mathf.Rad2Deg;

            if (!(alpha >= HeadHitMinAngel)) return false;
            bottomPlayer.currentPlayerState.DealDamage(5);
            return true;
        }

        private void Movement(Vector2 move, bool yMovement)
        {
            var distance = move.magnitude;

            if (distance > MinMoveDistance)
            {
                var count = player.rigidbody2D.Cast(move, _contactFilter2D, _hitBuffer, distance + ShellRadius);
                _hitBufferList.Clear();

                for (var i = 0; i < count; i++) _hitBufferList.Add(_hitBuffer[i]);

                foreach (var hit in _hitBufferList)
                {
                    var currentNormal = hit.normal;
                    if (currentNormal.y > minGroundNormalY)
                        if (yMovement)
                        {
                            _groundNormal = currentNormal;
                            currentNormal.x = 0;
                        }

                    var projection = Vector2.Dot(Velocity, currentNormal);
                    if (projection < 0) Velocity -= projection * currentNormal;

                    float modifiedDistance;

                    if (hit.distance <= 0)
                        modifiedDistance = Math.Abs(hit.distance - ShellRadius);
                    else
                        modifiedDistance = hit.distance - ShellRadius;

                    distance = modifiedDistance < distance ? modifiedDistance : distance;
                }
            }

            player.rigidbody2D.position += move.normalized * distance;
        }

        #region PlayerPlayerCollision

        private const float HeadHitMinAngel = 47f;
        public Vector2 playerHitVelocity;

        #endregion

        #region PhysicsGeneral

        protected Vector2 Velocity;
        private const float MinMoveDistance = 0.00085f;
        private const float ShellRadius = 0.008f;
        private readonly float minGroundNormalY = .65f;
        private Vector2 _groundNormal;

        public float gravityModifier = 1f;
        [Range(0, 1)] public float airFriction = 0.15f;
        [Range(0, 1)] public float groundFriction = 0.22f;
        [Range(0, 1)] public float downwardAirspeedMultiplayer = 0.025f;
        protected bool WantsToHangOnWall;

        #endregion

        #region PhysicsCollision

        private ContactFilter2D _contactFilter2D;
        private readonly RaycastHit2D[] _hitBuffer = new RaycastHit2D[16];
        private readonly List<RaycastHit2D> _hitBufferList = new List<RaycastHit2D>();

        #endregion
    }
}
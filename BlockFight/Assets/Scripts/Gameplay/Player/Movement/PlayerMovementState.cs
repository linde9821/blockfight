﻿using System;
using UnityEngine;

namespace Gameplay.Player.Movement
{
    /// <summary>
    ///     A class which holds all information for a players movement state
    /// </summary>
    public class PlayerMovementState : MonoBehaviour
    {
        public enum States
        {
            Idle,
            MovingOnGround,
            MovingInAirUp,
            MovingInAirDown,
            InAir,
            OnWallHangingLeft,
            OnWallSlidingLeft,
            OnWallHangingRight,
            OnWallSlidingRight
        }

        public States currentState;

        public LayerMask layerToCheckCollision;
        public float movingThreshold = 0.035f;

        private Animator _animator;

        private Vector2 _lastPosition;

        private States _lastState;

        private Player _player;

        private void Start()
        {
            _player = GetComponent<Player>();
            _animator = GetComponent<Animator>();

            currentState = States.Idle;
            _lastState = currentState;
            _lastPosition = _player.rigidbody2D.position;
        }

        private void Update()
        {
            // todo handle animation :D 
            switch (currentState)
            {
                case States.Idle:
                    //_animator.Play(IsIdling);
                    break;
                case States.MovingOnGround:
                    //_animator.Play(IsIdling);
                    break;
                case States.MovingInAirUp:
                    //_animator.Play(IsIdling);
                    break;
                case States.MovingInAirDown:
                    //_animator.Play(IsIdling);
                    break;
                case States.InAir:
                    //_animator.Play(IsIdling);
                    break;
                case States.OnWallHangingLeft:
                    //_animator.Play(IsIdling);
                    break;
                case States.OnWallSlidingLeft:
                    //_animator.Play(IsIdling);
                    break;
                case States.OnWallHangingRight:
                    //_animator.Play(IsIdling);
                    break;
                case States.OnWallSlidingRight:
                    //_animator.Play(IsIdling);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void FixedUpdate()
        {
            CalculateState();
            _lastPosition = _player.rigidbody2D.position;
            CheckForLanding();
            _lastState = currentState;
        }

        private void CheckForLanding()
        {
            if (IsInAirState(_lastState) && IsOnGroundState(currentState)) _animator.Play("Landing");
        }

        /// <summary>
        ///     Calculates the Players current state
        /// </summary>
        private void CalculateState()
        {
            if (IsOnGround())
            {
                if (IsMoving())
                {
                    currentState = States.MovingOnGround;
                    return;
                }

                currentState = States.Idle;
                return;
            }

            if (IsOnWallLeft())
            {
                if (IsMovingUp() || IsMovingDown())
                {
                    currentState = States.OnWallSlidingLeft;
                    return;
                }

                currentState = States.OnWallHangingLeft;
                return;
            }

            if (IsOnWallRight())
            {
                if (IsMovingUp() || IsMovingDown())
                {
                    currentState = States.OnWallSlidingRight;
                    return;
                }

                currentState = States.OnWallHangingRight;
                return;
            }

            if (IsMovingUp())
            {
                currentState = States.MovingInAirUp;
                return;
            }

            if (IsMovingDown())
            {
                currentState = States.MovingInAirDown;
                return;
            }

            if (!IsMoving())
            {
                currentState = States.InAir;
                return;
            }

            currentState = States.Idle;
        }

        private bool IsOnGround()
        {
            var position = transform.position;
            var leftX = position.x - _player.width / 2;
            var bottomY = position.y - _player.height / 2;

            for (var i = 0; i < 5; i++)
            {
                var start = new Vector2(leftX + _player.width / 4 * i, bottomY);

                if (CheckRaycast(start, Vector2.down).collider) return true;
            }

            return false;
        }

        private bool IsOnWallLeft()
        {
            var position = transform.position;
            var leftX = position.x - _player.width / 2;
            var bottomY = position.y - _player.height / 2;

            for (var i = 0; i < 4; i++)
            {
                var start = new Vector2(leftX, bottomY + _player.height / 4 * i);
                if (CheckRaycast(start, Vector2.left).collider) return true;
            }

            return false;
        }

        private bool IsOnWallRight()
        {
            var position = transform.position;
            var rightX = position.x + _player.width / 2;
            var bottomY = position.y - _player.height / 2;

            for (var i = 0; i < 4; i++)
            {
                var start = new Vector2(rightX, bottomY + _player.height / 4 * i);

                if (CheckRaycast(start, Vector2.right).collider) return true;
            }

            return false;
        }

        private bool IsMoving()
        {
            if (Vector2.Distance(_lastPosition, _player.rigidbody2D.position) >= movingThreshold) return true;

            return false;
        }

        private bool IsMovingUp()
        {
            return _player.rigidbody2D.position.y > _lastPosition.y;
        }

        private bool IsMovingDown()
        {
            return _player.rigidbody2D.position.y < _lastPosition.y;
        }

        private RaycastHit2D CheckRaycast(Vector2 startPosition, Vector2 direction)
        {
            Debug.DrawRay(startPosition, direction, Color.blue);
            return Physics2D.Raycast(startPosition, direction, _player.width / 10, layerToCheckCollision);
        }

        public bool IsInAir()
        {
            return IsInAirState(currentState);
        }

        private bool IsInAirState(States stateToCheck)
        {
            return stateToCheck == States.InAir || stateToCheck == States.MovingInAirDown ||
                   stateToCheck == States.MovingInAirUp;
        }

        private bool IsOnGroundState(States stateToCheck)
        {
            return stateToCheck == States.MovingOnGround || stateToCheck == States.Idle;
        }

        public bool isOnWall()
        {
            return currentState == States.OnWallHangingLeft || currentState == States.OnWallHangingRight ||
                   currentState == States.OnWallSlidingLeft || currentState == States.OnWallSlidingRight;
        }
    }
}
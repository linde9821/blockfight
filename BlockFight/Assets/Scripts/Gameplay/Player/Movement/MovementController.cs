﻿using System;
using Audio.AudioManager;
using Gameplay.Player.Physics;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace Gameplay.Player.Movement
{
    /// <summary>
    ///     A class which holds all movement logic
    /// </summary>
    public class MovementController : PhysicsObject
    {
        [FormerlySerializedAs("_isFacingRight")]
        public bool isFacingRight = true;

        public float currentMovementCooldown;
        public float dashCooldownTimer;
        private Animator _animator;
        private AudioManager _audioManager;

        private bool _canJump = true;

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        [Inject]
        public void Initialize(AudioManager audioManager)
        {
            _audioManager = audioManager;
        }

        protected override void ComputeVelocity()
        {
            WantsToHangOnWall = false;
            UpdateCooldownes();
            CheckIfCanJump();

            if (player.inputController.input.Shoot) return;

            if (player.inputController.input.Dash) HandleDash();

            if (currentMovementCooldown > 0) return;

            if (player.playerMovementState.isOnWall())
            {
                OnWallControl();
                return;
            }

            DefaultControl();

            if (player.inputController.inputDirection.x < 0)
                SetFacingLeft();
            else if (player.inputController.inputDirection.x > 0) SetFacingRight();
        }

        private void UpdateCooldownes()
        {
            var currentDeltaTime = Time.deltaTime;

            if (currentMovementCooldown <= 0)
                currentMovementCooldown = 0;
            else
                currentMovementCooldown -= currentDeltaTime;

            if (dashCooldownTimer <= 0)
                dashCooldownTimer = 0;
            else
                dashCooldownTimer -= currentDeltaTime;
        }

        private void CheckIfCanJump()
        {
            _canJump = player.playerMovementState.currentState == PlayerMovementState.States.Idle ||
                       player.playerMovementState.currentState == PlayerMovementState.States.MovingOnGround ||
                       player.playerMovementState.currentState == PlayerMovementState.States.OnWallHangingLeft ||
                       player.playerMovementState.currentState == PlayerMovementState.States.OnWallHangingRight ||
                       player.playerMovementState.currentState == PlayerMovementState.States.OnWallSlidingLeft ||
                       player.playerMovementState.currentState == PlayerMovementState.States.OnWallSlidingRight;
        }

        private void OnWallControl()
        {
            if (Math.Abs(player.inputController.inputDirection.x - -1) < 0.3f)
            {
                if (player.playerMovementState.currentState == PlayerMovementState.States.OnWallSlidingLeft ||
                    player.playerMovementState.currentState == PlayerMovementState.States.OnWallHangingLeft)
                    WantsToHangOnWall = true;

                Velocity.x = -player.playerStateValues.maxMovementSpeed;

                SetFacingLeft();
            }

            if (Math.Abs(player.inputController.inputDirection.x - 1) < 0.3f)
            {
                if (player.playerMovementState.currentState == PlayerMovementState.States.OnWallSlidingRight ||
                    player.playerMovementState.currentState == PlayerMovementState.States.OnWallHangingRight)
                    WantsToHangOnWall = true;

                Velocity.x = player.playerStateValues.maxMovementSpeed;

                SetFacingRight();
            }

            if (player.inputController.input.Jump && _canJump)
            {
                if (player.playerMovementState.currentState == PlayerMovementState.States.OnWallHangingLeft ||
                    player.playerMovementState.currentState == PlayerMovementState.States.OnWallSlidingLeft)
                {
                    Velocity.y = player.playerStateValues.wallJumpSpeed;
                    Velocity.x = player.playerStateValues.wallJumpSpeed;
                    currentMovementCooldown += player.playerStateValues.wallJumpMovementCooldown;

                    SetFacingRight();
                }
                else if (player.playerMovementState.currentState == PlayerMovementState.States.OnWallHangingRight ||
                         player.playerMovementState.currentState == PlayerMovementState.States.OnWallSlidingRight)
                {
                    Velocity.y = player.playerStateValues.wallJumpSpeed;
                    Velocity.x = -player.playerStateValues.wallJumpSpeed;
                    currentMovementCooldown += player.playerStateValues.wallJumpMovementCooldown;

                    SetFacingLeft();
                }

                _audioManager.Play("Jump");
            }
        }

        private void DefaultControl()
        {
            var currentMaxSpeed = player.playerStateValues.maxMovementSpeed;

            if (player.playerMovementState.IsInAir()) currentMaxSpeed *= player.playerStateValues.airControl;

            if (Math.Abs(player.inputController.inputDirection.x) > 0.1f)
                Velocity.x = player.inputController.inputDirection.x * currentMaxSpeed;

            if (!player.inputController.input.Jump || !_canJump) return;
            _audioManager.Play("Jump");
            Velocity.y = player.playerStateValues.jumpSpeed;
            currentMovementCooldown += player.playerStateValues.jumpCooldown;
            _canJump = false;
        }

        private void HandleDash()
        {
            if (dashCooldownTimer > 0) return;
            _audioManager.Play("Dash");
            bool dashTowardsFacingDirection;

            if (player.playerMovementState.isOnWall())
            {
                if (player.playerMovementState.currentState == PlayerMovementState.States.OnWallSlidingRight ||
                    player.playerMovementState.currentState == PlayerMovementState.States.OnWallHangingRight)
                    SetFacingLeft();

                if (player.playerMovementState.currentState == PlayerMovementState.States.OnWallSlidingLeft ||
                    player.playerMovementState.currentState == PlayerMovementState.States.OnWallHangingLeft)
                    SetFacingRight();

                dashTowardsFacingDirection = true;
            }
            else
            {
                Velocity = player.playerStateValues.dashSpeed * player.inputController.inputDirection;

                dashTowardsFacingDirection = Math.Abs(player.inputController.inputDirection.x) < 0.03f;
            }

            if (dashTowardsFacingDirection)
            {
                if (isFacingRight) Velocity.x = player.playerStateValues.dashSpeed;

                if (!isFacingRight) Velocity.x = -player.playerStateValues.dashSpeed;
            }

            currentMovementCooldown += player.playerStateValues.dashMovementCooldown;
            dashCooldownTimer += player.playerStateValues.dashCooldown;
            player.currentPlayerState.DamageResistenceTimer += player.playerStateValues.dashDamageResistenceTime;

            _animator.Play("Dash");
        }

        private void SetFacingRight()
        {
            if (!isFacingRight) Flip();
        }

        private void SetFacingLeft()
        {
            if (isFacingRight) Flip();
        }

        /// <summary>
        ///     swaps the current facing direction
        /// </summary>
        private void Flip()
        {
            isFacingRight = !isFacingRight;
            transform.localRotation = Quaternion.Euler(0, isFacingRight ? 0 : 180, 0);
        }
    }
}
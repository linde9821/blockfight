﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Gameplay.Player.Input
{
    /// <summary>
    ///     A class which holds all Input logic
    /// </summary>
    public class InputController : MonoBehaviour
    {
        public Vector2 inputDirection;
        public Inputs input;

        public void OnJump(InputAction.CallbackContext context)
        {
            if (context.performed)
                input.Jump = true;
            else if (context.canceled) input.Jump = false;
        }

        public void OnDash(InputAction.CallbackContext context)
        {
            if (context.performed)
                input.Dash = true;
            else if (context.canceled) input.Dash = false;
        }

        public void OnShoot(InputAction.CallbackContext context)
        {
            if (context.performed)
                input.Shoot = true;
            else if (context.canceled) input.Shoot = false;
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            inputDirection = context.ReadValue<Vector2>();
        }

        public struct Inputs
        {
            public bool Jump;
            public bool Dash;
            public bool Shoot;
        }
    }
}
﻿using System;
using Audio.AudioManager;
using GameManagment;
using Gameplay.Arrow;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace Gameplay.Player.Combat
{
    /// <summary>
    ///     A class which holds all logic for shooting an arrow
    /// </summary>
    public class ShootController : MonoBehaviour
    {
        private static Object _aimingArrowPrefab;

        private static Object _arrowPrefab;

        public LayerMask layerToCheckCollision;
        private GameObject _aimingArrow;

        private SpriteRenderer _aimingArrowRenderer;
        private AudioManager _audioManager;

        private float _currentShootCooldown;
        private GameManager _gameManager;
        private bool _lastShootInput;

        private Player _player;

        private void Start()
        {
            _player = GetComponent<Player>();
            _aimingArrowPrefab = Resources.Load("AimingArrow");
            _aimingArrow = Instantiate(_aimingArrowPrefab) as GameObject;
            _aimingArrowRenderer = _aimingArrow.GetComponent<SpriteRenderer>();
            SetAimingArrowRenderer(false);
            _lastShootInput = false;
            _currentShootCooldown = 0f;
        }

        private void Update()
        {
            UpdateShootCooldown();
            UpdateAimingArrow();
            CheckForShoot();
        }

        [Inject]
        public void Initialize(GameManager gameManager, AudioManager audioManager)
        {
            _gameManager = gameManager;
            _audioManager = audioManager;
        }

        private void UpdateShootCooldown()
        {
            if (_currentShootCooldown > 0)
                _currentShootCooldown -= Time.deltaTime;
        }

        private void UpdateAimingArrow()
        {
            if (_player.inputController.input.Shoot && _player.inputController.inputDirection.magnitude >= 0.8f)
                DisplayShootDirection();
            else
                SetAimingArrowRenderer(false);
        }

        public void SetAimingArrowRenderer(bool enabled)
        {
            _aimingArrowRenderer.enabled = enabled;
        }

        private void CheckForShoot()
        {
            if (_lastShootInput && !_player.inputController.input.Shoot && CanPlayerShoot() && !IsBlocked())
            {
                ShootArrow();
                _currentShootCooldown += _player.playerStateValues.shootCooldown;
            }

            _lastShootInput = _player.inputController.input.Shoot;
        }

        /// <summary>
        ///     Instantiate a arrow
        /// </summary>
        private void CreateArrow()
        {
            _arrowPrefab = Resources.Load("Arrow");
            var arrow = Instantiate(_arrowPrefab) as GameObject;
            var arrowController = arrow.GetComponent<ArrowController>();
            arrowController.SetDependencies(_audioManager, _gameManager, _player);
            arrowController.SetInitialDirection(_player.inputController.inputDirection);
        }

        private void ShootArrow()
        {
            CreateArrow();
        }

        private void DisplayShootDirection()
        {
            SetAimingArrowRenderer(true);
            Vector2 begin = _player.transform.position;
            var end = _player.inputController.inputDirection;

            _aimingArrowRenderer.color = CanPlayerShoot() ? Color.yellow : Color.grey;
            _aimingArrowRenderer.color = IsBlocked() ? Color.red : _aimingArrowRenderer.color;

            _aimingArrowRenderer.transform.position = begin + end.normalized * 0.4f;
            _aimingArrowRenderer.transform.localRotation = Quaternion.Euler(0, 0, RotateAimingArrow(end));
        }

        private bool IsBlocked()
        {
            var direction = _player.inputController.inputDirection;
            var position = _player.transform.position;
            return Physics2D.Raycast(position, direction, 0.4f, layerToCheckCollision).collider;
        }

        private bool CanPlayerShoot()
        {
            return _currentShootCooldown <= 0f && _player.inputController.inputDirection.magnitude >= 0.8f;
        }

        private float RotateAimingArrow(Vector2 vector)
        {
            var value = (float) (Mathf.Atan2(vector.y, vector.x) / Math.PI * 180f);
            if (value < 0) value += 360f;

            return value;
        }
    }
}
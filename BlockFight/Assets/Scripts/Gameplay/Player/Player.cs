﻿using Gameplay.Player.Combat;
using Gameplay.Player.Input;
using Gameplay.Player.Movement;
using Gameplay.Player.Stats;
using UnityEngine;
using UnityEngine.Serialization;

namespace Gameplay.Player
{
    /// <summary>
    ///     A class which holds references to all player components which allows other Scripts to access them.
    /// </summary>
    public class Player : MonoBehaviour
    {
        [FormerlySerializedAs("currentCharacterStats")] [HideInInspector]
        public CurrentPlayerState currentPlayerState;

        [HideInInspector] public float height;

        [HideInInspector] public InputController inputController;

        [HideInInspector] public MovementController movementController;

        [HideInInspector] public PlayerMovementState playerMovementState;

        [FormerlySerializedAs("playerStats")] [HideInInspector]
        public PlayerStateValues playerStateValues;

        [HideInInspector] public new Rigidbody2D rigidbody2D;

        [FormerlySerializedAs("shootBehaviour")]
        [FormerlySerializedAs("shootComponent")]
        [FormerlySerializedAs("shotComponent")]
        [FormerlySerializedAs("ShotComponent")]
        [HideInInspector]
        public ShootController shootController;

        [HideInInspector] public float width;

        public Color color;

        public void Awake()
        {
            Debug.LogWarning("Player is Initializing");
            playerStateValues = GetPlayerComponent<PlayerStateValues>();
            currentPlayerState = GetPlayerComponent<CurrentPlayerState>();

            movementController = GetPlayerComponent<MovementController>();
            playerMovementState = GetPlayerComponent<PlayerMovementState>();

            inputController = GetPlayerComponent<InputController>();

            rigidbody2D = GetPlayerComponent<Rigidbody2D>();

            shootController = GetPlayerComponent<ShootController>();

            width = GetPlayerComponent<Collider2D>().bounds.size.x;
            height = GetPlayerComponent<Collider2D>().bounds.size.y;
        }

        /// <summary>
        ///     Save alternative to GetComponent (throws an Exception when component couldn't be found and quits program)
        /// </summary>
        private T GetPlayerComponent<T>()
        {
            var component = GetComponent<T>();

            if (component == null)
            {
                Debug.LogError($"Couldn't get {typeof(T)} component");
                Application.Quit();
            }

            return component;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Audio.AudioManager;
using GameManagment;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Serialization;

namespace Gameplay.Arrow
{
    /// <summary>
    ///     A class which holds all arrow logic
    /// </summary>
    public class ArrowController : MonoBehaviour
    {
        private const float MinMovementDistance = 0.00085f;
        private const float OffsetRadius = 0.05f;

        [FormerlySerializedAs("despawneCooldownInSeconds")] [FormerlySerializedAs("despawneCooldown")]
        public float despawnCooldownInSeconds = 20f; // todo: add as decal setting maybe  

        public float gravityModifier = 1f;
        public Vector2 speedModifier = new Vector2(1.5f, 1.1f);
        [Range(0, 1)] public float timeScale = 0.7f;

        public Vector2 velocity;

        private readonly RaycastHit2D[] _hitBuffer = new RaycastHit2D[16];
        private readonly List<RaycastHit2D> _hitBufferList = new List<RaycastHit2D>();
        private AudioManager _audioManager;

        private PolygonCollider2D _collider;

        private ContactFilter2D _contactFilter2D;

        // managers are provided via ShootController
        private GameManager _gameManager;

        private bool _isFixed;

        private Light2D _light2D;
        private Material _material;

        private Player.Player _originPlayer;
        private Rigidbody2D _rigidbody2D;

        private void Awake()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
            _collider = GetComponent<PolygonCollider2D>();
            _light2D = GetComponent<Light2D>();
            _material = GetComponent<SpriteRenderer>().material;
            _contactFilter2D.useTriggers = false;
            _contactFilter2D.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
            _contactFilter2D.useLayerMask = true;
        }

        public void Update()
        {
            despawnCooldownInSeconds -= Time.deltaTime;
            CalculateRotation();
            if (CheckForDespawn()) DespawnArrow();
        }

        private void FixedUpdate()
        {
            if (_isFixed) return;

            velocity += Physics2D.gravity * gravityModifier;

            var deltaPosition = velocity * speedModifier * (Time.fixedDeltaTime * timeScale);

            Movement(deltaPosition);
        }

        private static Vector2 ComputeShotPosition(Vector2 position, Vector2 direction)
        {
            var begin = position;
            var end = direction;

            return begin + end.normalized * OffsetRadius;
        }

        /// <summary>
        ///     Sets the rotation of the arrow
        /// </summary>
        private void CalculateRotation()
        {
            transform.localRotation = Quaternion.Euler(0, 0, CalculateRotationAngleArrow());
        }

        private bool CheckForDespawn()
        {
            return despawnCooldownInSeconds <= 0 && _isFixed;
        }

        private void DespawnArrow()
        {
            Destroy(gameObject);
        }

        private void Movement(Vector2 move)
        {
            var distance = move.magnitude;

            if (distance > MinMovementDistance)
            {
                var count = _rigidbody2D.Cast(move, _contactFilter2D, _hitBuffer, distance);
                _hitBufferList.Clear();

                for (var i = 0; i < count; i++) _hitBufferList.Add(_hitBuffer[i]);

                foreach (var hit in _hitBufferList)
                    switch (hit.transform.gameObject.tag)
                    {
                        case "Player":
                            HandelPlayerHit(_gameManager.Players[hit.transform.gameObject]);
                            break;

                        case "Wall":
                            HandelWallHit();
                            break;
                    }
            }

            _rigidbody2D.position += move.normalized * distance;
        }

        private void HandelPlayerHit(Player.Player player)
        {
            Debug.LogWarning($"Arrow hitted {player.name}");
            player.currentPlayerState.DealDamage(7);
            player.movementController.playerHitVelocity =
                velocity.normalized * _originPlayer.playerStateValues.knockBackForce;
            _audioManager.Play("arrowHit");
            DespawnArrow();
        }

        private void HandelWallHit()
        {
            _isFixed = true;
            _collider.isTrigger = true;
            _light2D.intensity = 0.6f;
            _audioManager.Play("arrowImpact");
        }

        /// <summary>
        ///     Calculates an arrows direction based on the velocities vector direction
        /// </summary>
        private float CalculateRotationAngleArrow()
        {
            var value = (float) (Mathf.Atan2(velocity.y, velocity.x) / Math.PI * 180f);
            if (value < 0) value += 360f;

            return value;
        }

        private void SetInitialPosition()
        {
            Vector2 position = _originPlayer.transform.position;
            var direction = _originPlayer.inputController.inputDirection;

            transform.position = ComputeShotPosition(position, direction);
            transform.localRotation = Quaternion.Euler(0, 0, CalculateRotationAngleArrow());
        }

        public void SetInitialDirection(Vector2 direction)
        {
            velocity = direction.normalized * _originPlayer.playerStateValues.shootForce;
            CalculateRotation();
        }

        public void SetDependencies(AudioManager audioManager, GameManager gameManager, Player.Player player)
        {
            _audioManager = audioManager;
            _gameManager = gameManager;
            _originPlayer = player;

            _material.SetColor("_GlowColor", player.color);
            _light2D.color = player.color;

            SetInitialPosition();
            _audioManager.Play("arrowShoot");
        }
    }
}
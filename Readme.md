# BlockFight 1.0.0 (Uni-Version)

## Was ist BlockFight

BlockFight ist ein lokales Multiplayer Spiel für 2 bis 4 Spieler. Am besten spielt man es gemeinsam vor einem Bildschirm oder mit [Parsec](https://parsec.app/).  (Falls Sie niemanden haben um das Spiel zu testen stellen wir uns gern bereit. Kontaktieren sie uns einfach über Discord)

Zum beginn einer Runde wird für jeden Spieler ein Charakter gespawnt den dieser Steuern kann. (Achtung es **muss** die *Sprung / Join-Aktion* betätigt werden )
Das Ziel des Spiels ist es als letzter Spieler noch am Leben zu sein. 
Ein Spiel ist beendet wenn einer der Mitspieler nach dem "Best of X"-Prinzipe gewonnen hat. Es können drei, fünf oder sieben Runden eingestellt werden.

Man kann den Gegnern schaden zu fügen / töten erreichen indem man entweder:

1. Den Gegner mit einem Pfeil trifft
2. Auf den Gegner Heraufspringt (also eine Art Kick von oben)
3. Den Gegner aus der Arena drängt (Bspw. führt eine starke Kollision zu einem nur schwer kontrollierbaren Rückstoß)

Verteidigen kann man sich indem man den Pfeilen und Sprungangriffen ausweicht. Hilfreich ist dabei die Dash Funktion mit der man sich schneller Bewegen kann. Zusätzlich verleite einem der Dash eine kurzzeitige Resistenz gegen Pfeile in welcher man diese Absorbiert und keinen Schaden nimmt.

## Steuerung

Zum steuern **empfehlen wir einen Controller** aber es ist auch möglich mit der Tastatur zu spielen.
Da es ein lokales Multiplayer Spiel ist benötigt man also mindestens einen Controller und eine Tastatur um ein 2 Spieler Spiel zu beginnen.

Steuerungsschema:

Aktion | Controller-Steuerung | Tastatur-Steuerung
-------- | -------- | --------
*Sprung / Join*   | Süden (A oder X)   | Leertaste
*Dash*   | Rechter Trigger (R2)   | E
*Bewegen* | rechter Analogstick   | WASD
*Schießen* | West (X oder Kreis) + Bewegen um die Richtung festzulegen   | R + Bewegen um die Richtung festzulegen

Im Menü ist die Steuerung noch einmal zu finden.

## Maps

In der "Uni-Version" gibt es 2 Maps die vom Menü aus gestartet werden können (über dem Editor kann man theoretisch auch noch die 4 anderen developemnt maps öffnen). 

## Tipps

- Nicht nur ein Spieler kann durch ein Portal gehen sondern auch Pfeile
- Um sich an einer Wand fest zu halten kann man wenn man an dieser hängt die *Bewegen*-Aktion in Richtung der wand ausführen um sich daran festzuhalten   
- Ein Leben ist in 10 kleinere Leben eingeteilt. Diese kleinen Werte regenerieren sich mit der Zeit (nach 8 Sekunden wenn man keinen Schaden bekommt). Es kann also sinnvoll sein sich kurz zurückzuziehen
